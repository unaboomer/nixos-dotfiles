# Nixos Dotfiles
- My nixos configuration with Flakes and Home-Manager and NixVim

# Installation  
## 1. Add unstable channel
- Add repo with bigger amount of packages in
``` sh
nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixos
```
## 2. Cloning configuration
- Don't forget move this to /etc/nixos
``` sh
git clone https://gitlab.com/unaboomer/nixos-dotfiles
```
## 3. Rebuilding the new system
- Apply the configuration to new machine
``` sh
nix flake update /etc/nixos
nixos-rebuild -j 4 switch --upgrade --flake /etc/nixos/#unaboomer
```
# Useful Tips
## System rebuild
- Use this cmd to rebuild the system
``` sh
doas nixos-rebuild -j 4 switch --flake /etc/nixos/#unaboomer
```
## Search derivation
- Use this cmd to search for packages derivations
``` sh
nix edit nixpkgs#bat
```
