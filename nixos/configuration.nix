# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{...}: {
imports = [
  # Include the results of the hardware scan.
  # Generated during installation #
  ./modules/sys/hardware/hardware-configuration.nix
  # Nix-ld
  ./modules/sys/nix/nix-ld.nix
  # Nix settings
  ./modules/sys/nix/nix.nix
  ./modules/sys/nix/nixpkgs.nix
  # System
  ./modules/sys/boot.nix
  ./modules/sys/hardware.nix
  ./modules/sys/users.nix
  ./modules/sys/time.nix
  # Scripts
  ./modules/sys/scripts/scripts.nix
  # Environment
  ./modules/sys/variables.nix
  ./modules/sys/aliases.nix
  # Etc
  ./modules/sys/network.nix
  ./modules/sys/zram.nix
  ./modules/sys/console.nix
  ./modules/sys/fonts.nix
  # Apps
  ./modules/sys/packages.nix
  ./modules/sys/xorg.nix
  ./modules/sys/doas.nix
  ./modules/sys/zsh.nix
  ./modules/sys/pipewire.nix
  ./modules/sys/tlp.nix
  ./modules/sys/automount.nix
];
  
# Fast reboot
systemd = {
  extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';
};

# Select internationalisation properties.
i18n = {
  defaultLocale = "ru_RU.UTF-8";
};
   
# Dconf
programs.dconf.enable = true;

# Enable the OpenSSH daemon.
programs.ssh.askPassword = ""; # Disable x11-ssh-askpass
services.openssh.enable = true;

# This value determines the NixOS release from which the default
# settings for stateful data, like file locations and database versions
# on your system were taken. It‘s perfectly fine and recommended to leave
# this value at the release version of the first install of this system.
# Before changing this value read the documentation for this option
# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
system.stateVersion = "23.11"; # Did you read the comment?
}
