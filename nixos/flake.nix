{
  description = "Unaboomer NixOS configuration";

  inputs = {
    # Unstable channel #
    # Require 'doas nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixos'
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # Home Manager #
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Firefox Extensions #
    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # NixVim #
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs @ {
    nixpkgs,
    nixvim,
    self,
    home-manager,
    ...
  }: {
    nixosConfigurations = {
      unaboomer = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          # Main configuration #
          ./configuration.nix
          # Impermanence #
          #inputs.impermanence.nixosModules.impermanence
          # Disko #
          # Home Manager #
          inputs.home-manager.nixosModules.home-manager
	  {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users = {
                andrew = import ./home.nix;
              };
              extraSpecialArgs = {inherit inputs;};
            };
          }
        ];
      };
    };
  };
}
