{
  pkgs,
  inputs,
  ...
}: {
imports = [
  # Etc
  ./modules/hm/etc/xdg.nix
  ./modules/hm/etc/var.nix
  ./modules/hm/etc/keyboard.nix
  ./modules/hm/etc/X.nix
  # Shell
  ./modules/hm/shell/bash.nix
  ./modules/hm/shell/zsh.nix
  ./modules/hm/shell/zellij.nix
  # Apps
  ./modules/hm/app/fm/lf/lf.nix
  ./modules/hm/app/etc/git.nix
  ./modules/hm/app/etc/htop.nix
  ./modules/hm/app/www/firefox.nix
  ./modules/hm/app/terminal/alacritty.nix
  inputs.nixvim.homeManagerModules.nixvim
  ./modules/hm/app/editors/nixvim.nix
  ./modules/hm/app/media/mpv.nix
  ./modules/hm/app/media/yt-dlp.nix
  ./modules/hm/app/media/zathura.nix
  ./modules/hm/app/media/nsxiv/nsxiv_scripts.nix
  # WM #
  # X
  ./modules/hm/wm/bspwm/bspwm.nix
  # Wayland
  # ./modules/hm/wm/awesome/awesome.nix
  ./modules/hm/wm/xinitrc.nix
  ./modules/hm/wm/picom.nix
  ./modules/hm/wm/polybar.nix
  ./modules/hm/wm/dunst.nix
  # Themes
  ./modules/hm/themes/qt.nix
  ./modules/hm/themes/gtk.nix
  ./modules/hm/themes/pointer.nix
];

# Home Manager needs a bit of information about you and the paths it should manage.
programs.home-manager.enable = true;
home = {
  stateVersion = "23.11"; # Dont change it
  enableNixpkgsReleaseCheck = false;
  username = "andrew";
  homeDirectory = "/home/andrew";
};

# Clean home, but leave these directories untouched
# REQUIRE DISKO #
#home.persistence."/nix/persist/home" = {
#  directories = [
#    ".mozilla"
#    "docs/docs"
#    "docs/.desc"
#    "docs/dotfiles"
#    "docs/dow"
#    "docs/etc"
#    "docs/games"
#    "docs/mus"
#    "docs/nixos-dotfiles"
#    "docs/pic"
#    "docs/vid"
#    "docs/wal"
#  ];
#  allowOther = true;
#};

# The home.packages option allows you to install Nix packages into your
#environment.
home.packages = with pkgs; [
  (callPackage ./modules/hm/wm/dmenu.nix {})
  (callPackage ./modules/hm/app/media/nsxiv/nsxiv.nix {})
  p7zip
  tldr
  neofetch
  telegram-desktop
  libreoffice-qt
  xclip
  pdfgrep
  rsync
];

# Bluetooth #
# Enable headset buttons to control media players
services.mpris-proxy.enable = true;
}
