{ pkgs, ...}: {
programs.nixvim = {
  enable = true;
  enableMan = false; # Cause too long rebuild
  vimAlias = true;
  viAlias = true;
  extraPackages = with pkgs; [
    beautysh
    nixfmt-rfc-style
  ];
  extraPlugins = [
    (pkgs.vimUtils.buildVimPlugin {
      name = "tabby.nvim";
      src = pkgs.fetchFromGitHub {
        owner = "nanozuki";
        repo = "tabby.nvim";
        rev = "c4df244245e116280c961112cf6ee221ca3bc294";
        hash = "sha256-vV9I+PK6ixGrTcG+NGdDHg4nrHlpm/qhIYeyjC1s3FY=";
      };
    })
  ];
  extraConfigLua = ''
  -- Faster loading
  vim.loader.enable()
  -- Tabby.nvim --
  local theme = {
    fill = 'TabLineFill',
    -- Also you can do this: fill = { fg='#f2e9de', bg='#907aa9', style='italic' }
    head = 'TabLine',
    current_tab = 'TabLineSel',
    tab = 'TabLine',
    win = 'TabLine',
    tail = 'TabLine',
  }
  require('tabby.tabline').set(function(line)
    return {
    {
      { '  ', hl = theme.head },
      line.sep('', theme.head, theme.fill),
    },
    line.tabs().foreach(function(tab)
      local hl = tab.is_current() and theme.current_tab or theme.tab
      return {
        line.sep('', hl, theme.fill),
        tab.is_current() and '' or '󰆣',
        tab.number(),
        tab.name(),
        tab.close_btn(''),
        line.sep('', hl, theme.fill),
        hl = hl,
        margin = ' ',
      }
    end),
    line.spacer(),
    line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
      return {
        line.sep('', theme.win, theme.fill),
        win.is_current() and '' or '',
        win.buf_name(),
        line.sep('', theme.win, theme.fill),
        hl = theme.win,
        margin = ' ',
      }
    end),
    {
      line.sep('', theme.tail, theme.fill),
      { '  ', hl = theme.tail },
    },
    hl = theme.fill,
  }
end)
  -- Comment with TreeSitter --
  require('Comment').setup {
    pre_hook = require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook(),
  }
  '';
  clipboard.register = "unnamedplus";
  colorschemes.gruvbox.enable = true;
  globals = {
    mapleader = ",";
  };
  ### OPTIONS ###
  opts = {
    number = true;
    cursorline = true;
    undofile = true;
    ignorecase = true;
    smartcase = true;
    hlsearch = true;
    showtabline = 2;
    fillchars = "eob: ";
  };
  ### HOTKEYS ###
  keymaps = [
    # UndoTree
    {
      action = "<cmd>UndotreeToggle<CR>";
      key = "<leader>u";
      options = {
        silent = true;
      };
    }
    # Telescope
    {
      action = "<cmd>Telescope<CR>";
      key = "<leader>g";
      options = {
        silent = true;
      };
    }
    # Tabby.nvim
    {
      action = "<cmd>tabnew<CR>";
      key = "<leader><Up>";
    }
    {
      action = "<cmd>tabclose<CR>";
      key = "<leader><Down>";
    }
    {
      action = "<cmd>tabn<CR>";
      key = "<leader><Right>";
    }
    {
      action = "<cmd>tabp<CR>";
      key = "<leader><Left>";
    }
    # Trouble
    {
      action = "<cmd>TroubleToggle<CR>";
      key = "<leader>t";
    }
    # NeoTree
    {
      action = "<cmd>Neotree toggle<CR>";
      key = "<leader>n";
      options = {
        silent = true;
      };
    }
  ];
  ### PLUGINS ###
  plugins = {
    # Formatter #
    conform-nvim = {
      enable = true;
      formattersByFt = {
        nix = [ "nixfmt" ];
        bash = [ "beautysh" ];
      };
    };
    # Auto-tab #
    sleuth.enable = true;
    # Trailspace lines #
    #indent-blankline.enable = true;
    # Git ui
    neogit.enable = true;
    # Telescope #
    telescope = {
      enable = true;
    };
    # Last place
    lastplace.enable = true;
    # TreeSitter #
    # Base
    treesitter = {
      enable = true;
      ensureInstalled = [ "bash" "regex" "nix" ];
      indent = true;
    };
    # Undo Tree #
    undotree = {
      enable = true;
    };
    # Neo-tree #
    neo-tree = {
      enable = true;
      enableGitStatus = true;
      enableModifiedMarkers = true;
      enableRefreshOnWrite = true;
      sourceSelector = {
	winbar = true;
      };
    };
    # Comment #
    comment.enable = true;
    ts-context-commentstring = {
      enable = true;
    };
    # Treesitter integration
    # LSP #
    trouble.enable = true;
    lsp = {
      enable = true;
      servers = {
	nil_ls = {
	  enable = true;
	  autostart = true;
	};
	bashls = {
	  enable = true;
	  autostart = true;
	};
      };
    };
    # Icons fo LSP
    lspkind = {
      enable = true;
      mode = "text_symbol";
      preset = "default";
    };
    # CMP completion #
    ## Lsp integration
    cmp-nvim-lsp.enable = true;
    # CMP Ripgrep based completion
    cmp-rg.enable = true;
    # CMP TreeSitter based completion
    cmp-treesitter.enable = true;
    # CMP for Neovim-Lua
    cmp-nvim-lua.enable = true;
    ## Base CMP setup
    cmp = {
      enable = true;
      autoEnableSources = true;
      settings = {
        sources = [
	  {name = "nvim_lsp";}
	  {name = "treesitter";}
	  {name = "buffer";}
          {name = "path";}
        ];
	mapping = {
          __raw = ''
            cmp.mapping.preset.insert({
              ['<C-b>'] = cmp.mapping.scroll_docs(-4),
              ['<C-f>'] = cmp.mapping.scroll_docs(4),
              ['<C-Space>'] = cmp.mapping.complete(),
              ['<C-e>'] = cmp.mapping.abort(),
              ['<CR>'] = cmp.mapping.confirm({ select = true }),
            })
          '';
        };
      };
    };
    # Status Line #
    lualine = {
      enable = true;
      globalstatus = true;
      iconsEnabled = false;
      theme = "gruvbox-material";
    };
    # Mini.nvim plugins
    mini = {
      enable = true;
      modules = {
        starter = {};
        animate = {};
        pairs = {};
        cursorword = {};
        indentscope = {
          symbol = "│";
        };
      };
    };
    # Which-key plugin
    which-key = {
      enable = true;
    };
    # Rainbow-delimiters plugin
    rainbow-delimiters = {
      enable = true;
    };
    # Git diff
    gitsigns = {
      enable = true;
      #signs = {
	# add.text = "+";
	# change.text = "~";
	# delete.text = "-";
	# changedelete.text = "~";
      # };
    };
    # Show color tags
    nvim-colorizer = {
      enable = true;
    };
  };
};

}
