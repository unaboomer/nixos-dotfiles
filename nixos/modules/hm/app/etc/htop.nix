{pkgs, ...}: {
programs.htop = {
  enable = true;
  package = pkgs.htop-vim;
  settings = {
    color_scheme = 6;
    cpu_count_from_one = 1;
    highlight_base_name = 1;
    highlight_megabytes = 1;
    tree_view = 1;
  };
};

}
