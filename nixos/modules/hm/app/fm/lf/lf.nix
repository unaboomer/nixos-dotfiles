{pkgs, ...}: let
# UEBERZUG SCRIPT #
draw_img = pkgs.writeShellScript "draw_img.sh" ''
if [ -n "$FIFO_UEBERZUG" ]; then
  path="$(printf '%s' "$1" | sed 's/\\/\\\\/g;s/"/\\"/g')"
  printf '{"action": "add", "identifier": "preview", "x": %d, "y": %d, "width": %d, "height": %d, "scaler": "contain", "scaling_position_x": 0.5, "scaling_position_y": 0.5, "path": "%s"}\n' \
    "$4" "$5" "$2" "$3" "$1" >"$FIFO_UEBERZUG"
fi
'';
# CLEANER SCRIPT #
cleaner = pkgs.writeShellScript "clear_img.sh" ''
if [ -n "$FIFO_UEBERZUG" ]; then
  printf '{"action": "remove", "identifier": "preview"}\n' >"$FIFO_UEBERZUG"
fi
'';
# PREVIEWIER SCRIPT #
previewer = pkgs.writeShellScript "previewer.sh" ''
draw() {
  ${draw_img} "$@"
  #~/.config/lf/draw_img.sh "$@"
  exit 1
}
hash() {
  printf $XDG_CACHE_HOME'/lf/%s' "$HOME" \
    "$(${pkgs.coreutils}/bin/stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | awk '{print $1}')"
}
cache() {
  if [ -f "$1" ]; then
    draw "$@"
  fi
}
file="$1"
shift
if [ -n "$FIFO_UEBERZUG" ]; then
  case "$(file -Lb --mime-type -- "$file")" in
    image/*)
      orientation="$(identify -format '%[EXIF:Orientation]\n' -- "$file")"
      if [ -n "$orientation" ] && [ "$orientation" != 1 ]; then
        cache="$(hash "$file").jpg"
        cache "$cache" "$@"
        convert -- "$file" -auto-orient "$cache"
        draw "$cache" "$@"
      else
        draw "$file" "$@"
      fi
    ;;
    video/*)
      cache="$(hash "$file").jpg"
      cache "$cache" "$@"
      ffmpegthumbnailer -i "$file" -o "$cache" -s 0
      draw "$cache" "$@"
    ;;
    text/*)
      #bat -fP --terminal-width "$(($lf_width-2))" "$@"
      bat --terminal-width "$(($lf_width-2))" -f "$1" || cat "$1" 
    ;;
    audio/*)
      ${pkgs.mediainfo}/bin/mediainfo "$1" || exit 1
    ;;
  esac
fi
#file -Lb -- "$1" | fold -s -w "$lf_width"
exit 0
'';
in
{
# LF #
# Packages
home.packages = with pkgs; [
  ueberzugpp
  ffmpegthumbnailer
  mediainfo
  perl538Packages.FileMimeInfo
];
# Icons
home.file.".config/lf/icons" = {
  enable = true;
  source = ./icons;
};
# Colors
home.file.".config/lf/colors" = {
  enable = true;
  source = ./colors;
};
# Status bar
programs.eza = {enable = true;};
programs.bat = {enable = true;};
# LF
programs.lf = {
  enable = true;
  settings = {
    shell = "zsh";
    shellopts = "-eu";
    #ifs = "\n";
    #cleaner = "${cleaner}";
    #previewer = "${previewer}";
    preview = true;
    drawbox = true;
    ignorecase = true;
    scrolloff = 10;
    period = 1;
    mouse = true;
    sixel = true;
  };
  commands = {
    touch = ''%touch "$@"'';
    mkdir = ''%mkdir "$@"'';
    on-select = ''&lf -remote "send $id set statfmt '$(${pkgs.eza}/bin/exa -ld --color=always "$f")'"'';
    share = ''$curl -F"file=@$fx" https://0x0.st | xclip -selection c'';
    follow_link = ''&lf -remote "send ''${id} select '$(readlink $f)'"'';
    term-title = ''&printf "\033]0; lf - $PWD\007" > /dev/tty'';
    yank-path = ''$printf "%s" "$fx" | xclip -i -selection clipboard'';
    bulk-rename = ''
    ''${{
    old="$(mktemp)"
    new="$(mktemp)"
    if [ -n "$fs" ]; then
        fs="$(basename -a $fs)"
    else
        fs="$(ls -a)"
    fi
    printf '%s\n' "$fs" >"$old"
    printf '%s\n' "$fs" >"$new"
    nvim "$new"
    [ "$(wc -l < "$new")" -ne "$(wc -l < "$old")" ] && exit
    paste "$old" "$new" | while IFS= read -r names; do
        src="$(printf '%s' "$names" | cut -f1)"
        dst="$(printf '%s' "$names" | cut -f2)"
        if [ "$src" = "$dst" ] || [ -e "$dst" ]; then
            continue
        fi
        mv -- "$src" "$dst"
    done
    rm -- "$old" "$new"
    lf -remote "send $id unselect"
    }}
    '';
    tar = ''
      ''${{
      mkdir $1
      cp -r $fx $1
      tar czvf $1.tar.gz $1
      rm -rf $1
    }}       
    '';
    zip = ''
      ''${{
      set -f
      mkdir $1
      cp -r $fx $1
      ${pkgs.p7zip}/bin/7z a $1.zip $1
      rm -rf $1
      }}       
    '';
    extract = ''
      ''${{
      set -f
      case $f in
        *.zip) ${pkgs.p7zip}/bin/7z x $f ;;
        *.7z) ${pkgs.p7zip}/bin/7z x $f ;;
        *.rar) ${pkgs.p7zip}/bin/7z x $f ;;
        *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) ${pkgs.busybox}/bin/tar xjvf $f ;;
        *.tar.gz|*.tgz) ${pkgs.busybox}/bin/tar xzvf $f ;;
        *.tar.xz|*.txz) ${pkgs.busybox}/bin/tar xJvf $f ;;
      }}
    '';
    open = ''
    &{{
      case $(file --mime-type -Lb $f) in
        text/*) nvim $fx ;;
        image/*) nsxiv $fx ;;
        video/*) mpv $fx ;;
        audio/*) mpv $fx ;;
        *) mime-open $fx ;;
      esac
    }}
    '';
  };
  keybindings = {
    "<c-up>" = "top<enter>";
    "<c-down>" = "bottom<enter>";
    "<enter>" = "shell-async";
    "<backspace2>" = "set hidden!";
    a = "mkdir<space>";
    D = "delete";
    R = "bulk-rename";
    gL = "follow_link";
    W = "yank-path";
    E = "$doasedit $f";
    o = "$mimeopen --ask $f";
  };
  extraConfig = ''
    set previewer ${previewer}
    set cleaner ${cleaner}
    set ifs "\n"
    term-title
  '';
};
}
