{
  config,
  pkgs,
  ...
}: let
  ssimdownscale = pkgs.fetchurl {
    url = "https://gist.githubusercontent.com/igv/36508af3ffc84410fe39761d6969be10/raw/575d13567bbe3caa778310bd3b2a4c516c445039/SSimDownscaler.glsl";
    hash = "sha256-AEq2wv/Nxo9g6Y5e4I9aIin0plTcMqBG43FuOxbnR1w=";
  };
  fsr-upscale = pkgs.fetchurl {
    url = "https://gist.githubusercontent.com/agyild/82219c545228d70c5604f865ce0b0ce5/raw/2623d743b9c23f500ba086f05b385dcb1557e15d/FSR.glsl";
    hash = "sha256-VthZf8a3v20T+MOyvfHNxDsGF11RdGqrRM8dyhaSm54=";
  };
  adaptive-sharpen = pkgs.fetchurl {
    url = "https://gist.githubusercontent.com/igv/8a77e4eb8276753b54bb94c1c50c317e/raw/572f59099cd0e3eb5e321a6da0a3d90a7382e2dc/adaptive-sharpen.glsl";
    hash = "sha256-gn+z1mKsmpG0B16RF/5uHbwcBthZWbpxnNuVTft/uOQ=";
  };
in {
programs.mpv = {
  enable = true;
  scripts = with pkgs.mpvScripts; [
    thumbnail
    sponsorblock-minimal
  ];
  scriptOpts = {
    sponsorblock-minimal = {
      sponsorblock_minimal-server = "https://sponsor.ajay.app/api/skipSegments";
      sponsorblock_minimal-categories = ["sponsor"];
    };
    # shift+t
    mpv_thumbnail_script = {
      autogenerate = "yes";
      autogenerate_max_duration = "3600";
      prefer_mpv = "yes";
      mpv_no_sub = "no";
      thumbnail_width = 200;
      thumbnail_height = 200;
      thumbnail_count = 150;
      thumbnail_network = "no";
      background_color = "282828";
    };
  };
  bindings = {
    "b" = "change-list glsl-shaders toggle ${ssimdownscale}";
    "n" = "change-list glsl-shaders toggle ${adaptive-sharpen}";
    "m" = "change-list glsl-shaders toggle ${fsr-upscale}";
  };
  config = {
    audio-device = "alsa/pipewire";
    user-agent = "Mozilla/5.0";
    cache = "yes";
    osc = "no"; # For thumbnailer
    demuxer-max-bytes = "500M";
    demuxer-max-back-bytes = "100M";
    cache-pause = "yes";
    cache-on-disk = "yes";
    demuxer-cache-dir = "${config.xdg.cacheHome}/";
    volume-max = "300";
  };
};
}
