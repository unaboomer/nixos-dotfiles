{lib, conf ? null , pkgs ? import <nixpkgs> {}}:
# nix-build
pkgs.stdenv.mkDerivation {
  name = "nsxiv";
  src = pkgs.fetchFromGitLab {
    owner = "unaboomer";
    repo = "nsxiv";
    rev = "f6fe69a860115d23ac1625b5a123532b2c93ccc2";
    hash = "sha256-zbgY4txGEGJZdr1Tbt68Med9KbWg/06nKpBLoUwlids=";
  };
  
  outputs = [ "out" "man" "doc" ];

  buildInputs = with pkgs; [
    giflib
    imlib2
    xorg.libXft
    libexif
    libwebp
  ];

  postPatch = lib.optionalString (conf != null) ''
    cp ${(builtins.toFile "config.def.h" conf)} config.def.h
  '';

  makeFlags = [ "CC:=$(CC)" ];
  installFlags = [ "PREFIX=$(out)" ];
  installTargets = [ "install-all" ];
  
  #buildPhase = ''
  #  make -j4
  #'';
  #installPhase = ''
  #  mkdir -p $out/bin
  #  cp dmenu $out/bin
  #  cp dmenu_path $out/bin
  #  cp dmenu_run $out/bin
  #  cp stest $out/bin
  #'';
}
