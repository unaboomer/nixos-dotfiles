{ pkgs, ... }:

{
# Nsxiv status line #
home.file.".config/nsxiv/exec/image-info" = {
  enable = true;
  executable = true;
  recursive = true;
  source = pkgs.writeShellScript "image-info" ''
# Example for $XDG_CONFIG_HOME/nsxiv/exec/image-info
# Called by nsxiv(1) whenever an image gets loaded.
# The output is displayed in nsxiv's status bar.
# Arguments:
#   $1: path to image file (as provided by the user)
#   $2: image width
#   $3: image height
#   $4: fully resolved path to the image file
s=" | " # field separator
exec 2>/dev/null
filename=$(basename -- "$1")
filesize=$(du -Hh -- "$1" | cut -f 1)
geometry="''${2}x''${3}"
echo "''${filesize}''${s}''${geometry}''${s}''${filename}"
  '';
};
# Nsxiv key-handler #
home.file.".config/nsxiv/exec/key-handler" = {
  enable = true;
  executable = true;
  recursive = true;
  source = pkgs.writeShellScript "key-handler" ''
# Example for $XDG_CONFIG_HOME/nsxiv/exec/key-handler
# Called by nsxiv(1) after the external prefix key (C-x by default) is pressed.
# The next key combo is passed as its first argument. Passed via stdin are the
# images to act upon: all marked images, if in thumbnail mode and at least one
# image has been marked, otherwise the current image. nsxiv(1) will block until
# the handler terminates. It then checks which images have been modified and
# reloads them.

# By default nsxiv(1) will send one image per-line to stdin, however when using
# -0 the image list will be NULL separated and the environment variable
# "$NSXIV_USING_NULL" will be set to 1.

# The key combo argument has the following form: "[C-][M-][S-]KEY",
# where C/M/S indicate Ctrl/Meta(Alt)/Shift modifier states and KEY is the X
# keysym as listed in /usr/include/X11/keysymdef.h without the "XK_" prefix.
# If KEY has an uppercase equivalent, S-KEY is resolved into it. For instance,
# K replaces S-k and Scedilla replaces S-scedilla, but S-Delete is sent as-is.
rotate() {
	degree="$1"
	tr '\n' '\0' | xargs -0 realpath | sort | uniq | while read file; do
		case "$(file -b -i "$file")" in
		image/jpeg*) jpegtran -rotate "$degree" -copy all -outfile "$file" "$file" ;;
		*)           mogrify  -rotate "$degree" "$file" ;;
		esac
	done
}
case "$1" in
"C-x")      xclip -in -filter | tr '\n' ' ' | xclip -in -selection clipboard ;;
"C-c")      while read file; do xclip -selection clipboard -target image/png "$file"; done ;;
"C-g")      tr '\n' '\0' | xargs -0 gimp & ;;
"C-r")      while read file; do rawtherapee "$file" & done ;;
"C-comma")  rotate 270 ;;
"C-period") rotate  90 ;;
"C-slash")  rotate 180 ;;
esac
'';
};
# Nsxiv thumb-info #
home.file.".config/nsxiv/exec/thumb-info" = {
  enable = true;
  executable = true;
  recursive = true;
  source = pkgs.writeShellScript "thumb-info" ''
# Example for $XDG_CONFIG_HOME/nsxiv/exec/thumb-info
# Called by nsxiv(1) whenever the selected thumbnail changes.
# The output is displayed in nsxiv's status bar.
# Arguments:
#   $1: path to image file (as provided by the user)
#   $2: empty
#   $3: empty
#   $4: fully resolved path to the image file
s="  " # field separator
exec 2>/dev/null
filename=$(basename -- "$4")
filesize=$(du -Hh -- "$4" | cut -f 1)
echo "''${filesize}''${s}''${filename}"
  '';
};
# Window title #
home.file.".config/nsxiv/exec/win-title" = {
  enable = true;
  executable = true;
  recursive = true;
  source = pkgs.writeShellScript "win-title" ''
# Example for $XDG_CONFIG_HOME/nsxiv/exec/win-title
# Called by nsxiv(1) whenever any of the relevant information changes.
# The output is set as nsxiv's window title.
#
# Arguments, "Optional" arguments might be empty:
#   $1: resolved absolute path of the current file
#   $2: current file number
#   $3: total file number
#   $4: image width (Optional: Disabled on thumbnails mode)
#   $5: image height (Optional: Disabled on thumbnails mode)
#   $6: current zoom (Optional: Disabled on thumbnails mode)
#
# The term file is used rather than image as nsxiv does not
# precheck that the input files are valid images. Total file
# count may be different from the actual count of valid images.
exec 2>/dev/null
filename="''${1##*/}"
if [ -n "$4" ]; then # image mode
	printf "%s" "nsxiv - ''${filename} | ''${4}x''${5} ''${6}% [''${2}/''${3}]"
else
	printf "%s" "nsxiv - ''${filename} [''${2}/''${3}]"
fi
  '';
};

}
