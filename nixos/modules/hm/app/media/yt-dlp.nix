{...}:


{
programs.yt-dlp = {
  enable = true;
  settings = {
    embed-thumbnail = true;
    embed-metadata = true;
    embed-subs = true;
    #downloader = "aria2c";
    sponsorblock-api = "https://sponsor.ajay.app";
    sponsorblock-mark = "filler,poi_highlight";
    sponsorblock-remove = "sponsor";
    restrict-filenames = true;
    split-chapters = true;
  };
};
}
