{pkgs, ...}: {
programs.zathura = {
  enable = true;
  package = pkgs.zathura;
  options = {
    font = "Iosevka Nerd Font 14";
    selection-notification = "true";
    selection-clipboard = "clipboard";
    adjust-open = "width";
    pages-per-row = "2";
    database = "sqlite";
    # Gruvbox pallete #
    notification-error-bg = "#cc241d";
    notification-error-fg = "#ebdbb2";
    notification-warning-bg = "#fabd2f";
    notification-warning-fg = "#1d1f21";
    notification-bg = "#458588"; 
    notification-fg = "#ebdbb2";
    # Completion
    completion-bg = "#504945";
    completion-fg = "#ebdbb2";
    completion-group-bg = "#3c3836";
    completion-group-fg = "#928374";
    completion-highlight-bg = "#83a598";
    completion-highlight-fg = "#504945";
    # Index
    index-bg = "#504945";
    index-fg = "#ebdbb2";
    index-active-bg = "#83a598";
    index-active-fg = "#504945";
    # Input
    inputbar-bg = "#1d1f21";
    inputbar-fg = "#ebdbb2";
    # Status
    statusbar-bg = "#1d1f21";
    statusbar-fg = "#ebdbb2";
    # Highlight
    highlight-color = "#fabd2f";
    highlight-active-color = "#fe8019";
    # Etc
    default-bg = "#1d1f21";
    default-fg = "#ebdbb2";
    render-loading = "true";
    render-loading-bg = "#1d1f21";
    render-loading-fg = "#ebdbb2";
    # Statusbar
    statusbar-basename = true;
    statusbar-home-tilde = true;
    # Window title
    window-title-basename = true;
    window-title-home-tilde = true;
    window-title-page = true;
    # Recolor
    recolor = false;
    recolor-keephue = true;
    recolor-reverse-video = true;
    recolor-lightcolor = "#282828";
    recolor-darkcolor = "#ebdbb2";
  };
};
}
