{...}:

{

home.file = {
  ".config/alacritty/gruvbox_dark.toml" = {
    text =
''
[font]
size = 13.0

[font.normal]
family = 'Iosevka Nerd Font'
style = 'Regular'

[font.bold]
family = 'Iosevka Nerd Font'
style = 'Bold'

[font.bold_italic]
family = 'Iosevka Nerd Font'
style = 'Bold Italic'

[font.italic]
family = 'Iosevka Nerd Font'
style = 'Italic'

[colors.primary]
background = '#282828'
foreground = '#ebdbb2'

[colors.normal]
black = '#282828'
red = '#cc241d'
green = '#98971a'
yellow = '#d79921'
blue = '#458488'
magenta = '#b16286'
cyan = '#689d6a'
white = '#a89984'

[colors.bright]
black = '#928374'
red = '#fb4934'
green = '#b8bb26'
yellow = '#fabd2f'
blue = '#83a598'
magenta = '#d3869b'
cyan = '#8ec07c'
white = '#ebdbb2'
'';
  };
};

programs.alacritty = {
  enable = true;
  settings = {
    import = [ "~/.config/alacritty/gruvbox_dark.toml" ];
  };
};

}
