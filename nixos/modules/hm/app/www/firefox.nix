{
  pkgs,
  inputs,
  ...
}: {
programs.firefox = {
  enable = true;
  profiles.unaboomer = {
    # about:config
    settings = {
      "browser.display.use_document_fonts" = 0;
      "font.name.monospace.x-cyrillic" = "Iosevka Nerd Font";
      "font.name.sans-serif.x-cyrillic" = "Iosevka Nerd Font";
      "font.name.serif.x-cyrillic" = "Iosevka Nerd Font";
      "font.size.monospace.x-cyrillic" = "18";
      "font.size.variable.x-cyrillic" = "17";
      "font.name.monospace.western" = "Iosevka Nerd Font";
      "font.name.sans-serif.western" = "Iosevka Nerd Font";
      "font.name.serif.wester" = "Iosevka Nerd Font";
      "font.name.serif.x-western" = "Iosevka Nerd Font";
      "font.size.monospace.western" = "18";
      "font.size.variable.western" = "17";
      "browser.vpn_promo.enabled" = "false";
      "extensions.pocket.enabled" = "false";
      "layout.spellcheckDefault" = "2"; # More spellcheck
      "config.trim_on_minimize" = "true"; # Lower memory usage
    };
    # 'nix flake show "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons"' to list extensions
    extensions = with inputs.firefox-addons.packages."x86_64-linux"; [
      auto-tab-discard
      clearurls
      darkreader
      gruvbox-dark-theme
      decentraleyes
      disconnect
      duckduckgo-privacy-essentials
      fastforwardteam
      firefox-translations
      i-dont-care-about-cookies
      istilldontcareaboutcookies
      noscript
      user-agent-string-switcher
      single-file
      stylus
      tabliss
      ublock-origin
      ublock-origin-lite
      vimium-c
      violentmonkey
      wayback-machine
    ];
    search = {
      default = "DuckDuckGo";
      force = true;
      engines = {
        "Nix Packages" = {
          urls = [
            {
              template = "https://search.nixos.org/packages";
              params = [
                {
                  name = "type";
                  value = "packages";
                }
                {
                  name = "query";
                  value = "{searchTerms}";
                }
              ];
            }
          ];
          icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
          definedAliases = ["@np"];
        };
        "NixOS Wiki" = {
          urls = [
            {
              template = "https://nixos.wiki/index.php?search={searchTerms}";
            }
          ];
          iconUpdateURL = "https://nixos.wiki/favicon.png";
          updateInterval = 24 * 60 * 60 * 1000; # every day
          definedAliases = ["@nw"];
        };
      };
    };
  };
  policies = {
    # 'about:policies#documentation'
    DefaultDownloadDirectory = "\${home}/docs/dow";
    PromptForDownloadLocation = false;
    DisableFeedbackCommands = true;
    DisableProfileImport = true;
    DisableProfileRefresh = true;
    DisableSetDesktopBackground = true;
    DontCheckDefaultBrowser = true;
    HardwareAcceleration = true;
    NoDefaultBookmarks = true;
    OverrideFirstRunPage = "";
    PrintingEnabled = false;
    ShowHomeButton = false;
    DisablePocket = true;
    DisableTelemetry = true;
    DisableFirefoxScreenshots = true;
  };
};
}
