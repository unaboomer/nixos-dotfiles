{...}:

{

# Xresources
xresources = {
  path = ".config/X11/Xresources";
};
# Xsession
xsession = {
  enable = true;
  numlock = {
    enable = true;
  };
  profilePath = ".config/X11/xprofile";
  scriptPath = ".config/X11/xsession";
};

}
