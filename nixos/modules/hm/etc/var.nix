{config, ...}: {
home.sessionVariables = {
  EDITOR = "nvim";
  BROWSER = "firefox";
  TERMINAL = "alacritty";
    MANPAGER = ''nvim +Man!'';
  XDG_DATA_HOME = "${config.home.homeDirectory}/.local/share";
  XAUTHORITY = "/run/user/1000/Xauthority";
  XINITRC = "${config.home.homeDirectory}/.config/X11/xinitrc";
  ZDOTDIR = "${config.home.homeDirectory}/.config/zsh";
  WINEPREFIX = "${config.home.homeDirectory}/docs/etc/.wine";
  XCURSOR_PATH = "${config.home.homeDirectory}/.local/share/icons";
  TLDR_CACHE_DIR = "${config.home.homeDirectory}/docs/etc/.cache/tldr";
  #XDG_DATA_DIRS="$HOME/.nix-profiles/share:${XDG_DATA_DIRS}";
  #XDG_CONFIG_HOME="${config.home.homeDirectory}/.config";
  XDG_STATE_HOME = "${config.home.homeDirectory}/.local/state";
};
}
