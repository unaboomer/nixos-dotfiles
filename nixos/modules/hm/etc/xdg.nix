{config, ...}: {
# Make programs prefer XDG Base Directories
home.preferXdgDirectories = true;
   
xdg = {
  enable = true;
  mime = {
    enable = true;
  };
  # Mime Associations #
  ## Get file type "image/png"
  # xdg-mime query filetype pic-part-2024.03.25-22:57:53.png
  ## Query program "feh.desktop"
  # xdg-mime query default image/png
  ## List "*.desktop" files
  # ls /etc/profiles/per-user/andrew/share/applications
  mimeApps = {
    enable = true;
    defaultApplications = {
      "text/plain" = ["nvim.desktop"];
      "x-scheme/tg" = ["org.telegram.desktop"];
      "x-scheme/http" = ["firefox.desktop"];
      "x-scheme/https" = ["firefox.desktop"];
      "application/pdf" = ["org.pwmt.zathura.desktop"];
      "video/quicktime" = ["mpv.desktop"];
      "videos/mp4" = ["mpv.desktop"];
      "image/png" = ["nsxiv.desktop"];
      "image/jpg" = ["nsxiv.desktop"];
      "image/jpeg" = ["nsxiv.desktop"];
      "image/heif" = ["nsxiv.desktop"];
    };
  };
  cacheHome = "${config.home.homeDirectory}/docs/etc/.cache";
  userDirs = {
    enable = true;
    createDirectories = true;
    desktop = "${config.home.homeDirectory}/docs/.desc";
    documents = "${config.home.homeDirectory}/docs/docs";
    download = "${config.home.homeDirectory}/docs/dow";
    extraConfig = {XDG_MISC_DIR = "${config.home.homeDirectory}/docs/etc";};
    music = "${config.home.homeDirectory}/docs/mus";
    pictures = "${config.home.homeDirectory}/docs/pic";
    videos = "${config.home.homeDirectory}/docs/vid";
    publicShare = "${config.home.homeDirectory}/docs/etc/.public";
    templates = "${config.home.homeDirectory}/docs/etc/.templates";
    # Custom dirs to create
    extraConfig = {
      XDG_SCR_DIR = "${config.home.homeDirectory}/docs/pic/scr";
    };
  };
};
# User dirs locale
home.file = {"${config.xdg.configHome}/user-dirs.locale" = {text = ''en_EN'';};};
}
