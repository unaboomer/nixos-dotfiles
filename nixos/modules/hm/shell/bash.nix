{config, ...}: {
programs.bash = {
  enable = true;
  historyFile = "${config.home.homeDirectory}/.local/share/bash_history";
};
}
