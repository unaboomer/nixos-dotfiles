{...}: {
programs.zellij = {
  enable = true;
  #enableZshIntegration = true;
  settings = {
    session_serialization = true;
    simplified_ui = true;
    theme = "gruvbox-dark";
    default_shell = "zsh";
    copy_command = "xclip -selection clipboard";
  };
};
}
