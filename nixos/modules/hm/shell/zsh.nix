{
  config,
  pkgs,
  ...
}: {
home.packages = with pkgs; [
    zsh-completions
  ];
programs.zsh = {
  enable = true;
  # history
  history = {
    path = "${config.home.homeDirectory}/.config/zsh/zsh_history";
    save = 100000;
    share = true;
  };
  # syntax highlight
  syntaxHighlighting = {
    enable = true;
    package = pkgs.zsh-syntax-highlighting;
  };
  # suggestions
  autosuggestion = {
    enable = true;
    #highlight = "fg=#98971a,bold,underline";
  };
  # completion
  enableCompletion = true;
  dotDir = ".config/zsh";
  # aliases
  shellAliases = {
    lf = "lfub";
    rg = "rga";
    cat = "bat --paging=never";
  };
  # Environment
  initExtra = ''
    # Colors
    autoload -U colors && colors
    # Completion
    autoload -U compinit && compinit
    zstyle ':completion:*' menu select
    zmodload zsh/complist
    _comp_options+=(globdots)
    # Autosuggest
    export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#98971a,bold,underline"
    export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
    # Profiling
    zmodload zsh/zprof
    # Prompt
    PS1="%b%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%m %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
    echo '\e[5 q'
    function cheat() {
      # Ask cheat.sh website for details about a Linux command.
      curl -m 10 "http://cheat.sh/$1" 2>/dev/null || printf '%s\n' "[ERROR] Something broke"
    }
  '';
};
}
