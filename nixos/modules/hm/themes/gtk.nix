{
  config,
  pkgs,
  ...
}:
let
  gruvboxPlus = import ./gruvbox-plus.nix { inherit pkgs; };
in {
home.file = {
  ".local/share/icons/GruvboxPlus".source = "${gruvboxPlus}";
};
gtk = {
  enable = true;
  # Cursor
  cursorTheme = {
    package = pkgs.capitaine-cursors-themed;
    name = "Capitaine Cursors (Gruvbox)";
  };
  # Theme
  theme = {
    package = pkgs.gruvbox-dark-gtk;
    name = "gruvbox-dark";
  };
  # Icons
  iconTheme = {
    package = gruvboxPlus;
    name = "Gruvbox-Plus-Dark";
  };
  # Font
  font = {
    name = "Iosevka Nerd Font";
    size = 14;
  };
  gtk2 = {
    configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
    extraConfig = ''
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle="hintmedium"
      gtk-enable-animation=1
      gtk-toolbar-style=GTK_TOOLBAR_BOTH
      gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
      gtk-button-images=1
      gtk-menu-images=1
    '';
  };
};
}
