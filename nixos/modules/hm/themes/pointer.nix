{pkgs, ...}:

{
home.pointerCursor = {
  #gtk.enable = true;
  x11.enable = true;
  package = pkgs.capitaine-cursors-themed;
  name = "Capitaine Dark";
  size = 18;
};
}
