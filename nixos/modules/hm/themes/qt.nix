{pkgs, ...}: {
qt = {
  enable = true;
  platformTheme = "gtk";
  style.name = "Fusion";
  style.package = pkgs.gruvbox-dark-gtk;
};
}
