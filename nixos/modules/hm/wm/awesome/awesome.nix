{
  pkgs,
  ... 
}: {

#home.file.".config/awesome/rc.lua" = {
#  enable = true;
#  recursive = true;
#  source = ./rc.lua;
#};

xsession.windowManager.awesome = {
  enable = false;
  luaModules = with pkgs.luajitPackages; [
    luarocks
    luadbi
    vicious
  ];
};

}
