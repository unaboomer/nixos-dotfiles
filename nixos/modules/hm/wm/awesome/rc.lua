-- awesome_mode: api-level=4:screen=on
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
require("widgets") --Bar widgets
local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local battery_widget = require("battery-widget")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
-- Declarative object management
local ruled = require("ruled")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Rubato
local rubato = require("rubato")
-- Lain
local lain = require("lain")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
--require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Ууупс, произошла ошибка"..(startup and " во время запуска!" or "!"),
        message = message
    }
end)
-- }}}

-- {{{ Variables
-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_themes_dir() .. "zenburn/theme.lua")
beautiful.font = "Iosevka Nerd Font 14"
-- Hotkeys colors
beautiful.hotkeys_font = "Iosevka Nerd Font 14"
beautiful.hotkeys_description_font = "Terminess Nerd Font 14"
beautiful.hotkeys_fg = "#F0DFAF"
beautiful.hotkeys_bg = "#282828"
beautiful.hotkeys_modifiers_fg = "#928374"

local bling = require("bling")
-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor = os.getenv("EDITOR") or "neovim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
-- }}}

-- {{{ Bling
-- Window swallowig
bling.module.window_swallowing.start()
bling.module.flash_focus.enable()
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
beautiful.menu_height = 15
beautiful.menu_width = 130
awesomemenu = {
   { "󰌌 Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "󰋗 Manual", terminal .. " -e man awesome" },
   { " Edit config", editor_cmd .. " " .. awesome.conffile },
   { "󰜉 Restart", awesome.restart },
   { "󰗼 Quit", function() awesome.quit() end },
}

mainmenu = awful.menu({ items = { { "Awesome", awesomemenu, beautiful.awesome_icon },
                                  { " Open terminal", terminal }
                                  }
                     })
launcher = awful.widget.launcher({ image = beautiful.awesome_icon,
				   menu = mainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Tag layout
-- Table of layouts to cover with awful.layout.inc, order matters.
tag.connect_signal("request::default_layouts", function()
    awful.layout.append_default_layouts({
	awful.layout.suit.tile,
	awful.layout.suit.floating,
	--awful.layout.suit.tile.left,
        --awful.layout.suit.tile.bottom,
        --awful.layout.suit.tile.top,
	--awful.layout.suit.fair,
        --awful.layout.suit.fair.horizontal,
        --awful.layout.suit.spiral,
        --awful.layout.suit.spiral.dwindle,
        awful.layout.suit.max,
        --awful.layout.suit.max.fullscreen,
        --awful.layout.suit.magnifier,
        --awful.layout.suit.corner.nw,
    })
end)

-- }}}

-- {{{ Random Wallpapers
-- Get the list of files from a directory. Must be all images or folders and non-empty.
   function scanDir(directory)
	local i, fileList, popen = 0, {}, io.popen
	for filename in popen([[find "]] ..directory.. [[" -type f]]):lines() do
	    i = i + 1
	    fileList[i] = filename
	end
	return fileList
   end
   wallpaperList = scanDir("/home/andrew/docs/pic/wal")
-- Apply a random wallpaper on startup.
   gears.wallpaper.maximized(wallpaperList[math.random(1, #wallpaperList)], s, true)
-- }}}

-- {{{ Wibar
-- Keyboard map indicator and switcher
keyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
local textclock = wibox.widget {
    format = '%H:%M|%d/%m',
    widget = wibox.widget.textclock,
}

screen.connect_signal("request::desktop_decoration", function(s)
    -- Each screen has its own tag table.
    awful.tag.add("1", {
    --icon               = "/path/to/icon1.png",
    name = "1",
    layout             = awful.layout.suit.tile,
})

awful.tag.add("2", {
    name = "2",
    --icon = "/path/to/icon2.png",
    layout = awful.layout.suit.tile,
})

awful.tag.add("3", {
    name = "3",
    --icon               = "/path/to/icon1.png",
    layout             = awful.layout.suit.tile,
})

awful.tag.add("4", {
    name = "4",
    --icon = "/path/to/icon2.png",
    layout = awful.layout.suit.tile,
})

awful.tag.add("5", {
    name = "5",
    --icon = "/path/to/icon2.png",
    layout = awful.layout.suit.tile,
})

    --local names = { "main", "www", "skype", "gimp", "office", "vim" }
    --local l = awful.layout.suit  -- Just to save some typing: use an alias.
    --local layouts = { l.floating, l.tile, l.floating, l.fair, l.max, l.floating, l.tile.left,
    --l.floating, l.floating }
    --awful.tag(names, s, layouts)

    -- Create a promptbox for each screen
    s.promptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.layoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc(-1) end),
            awful.button({ }, 5, function () awful.layout.inc( 1) end),
        }
    }

    -- Create a taglist widget
    taglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
	--style = { -- Rounding
	--  shape = gears.shape.rounded_rect
	--},
	buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t)
                                            if client.focus then
                                                client.focus:move_to_tag(t)
                                            end
                                        end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3, function(t)
                                            if client.focus then
                                                client.focus:toggle_tag(t)
                                            end
                                        end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        }
    }

    -- Brightness level
    local brightness = brightness_widget{
            type = 'icon_and_text',
            program = 'brightnessctl',
            step = 5,
        }

    -- Volume level
    local volume = volume_widget{
            widget_type = 'arc',
	    device = "alsa",
        }

    -- Create bar separator
    sep = wibox.widget.textbox ("|")

    -- Battery
    local battery = battery_widget {
       ac = "AC",
       adapter = "BAT0",
       ac_prefix = "AC:",
       battery_prefix = "Bat: ",
       percent_colors = {
	  { 25, "#cc241d"},
	  { 50, "#d65d0e"},
	  { 75, "#d79921"},
	  {999, "#98971a"},
       },
       listen = true,
       timeout = 10,
       widget_text = "${AC_BAT}${color_on}${percent}%${color_off}",
       widget_font =  beautiful.font,
       tooltip_text = "Батарея ${state}${time_est}\nЗаряд: ${capacity_percent}%",
       alert_threshold = 3,
       alert_timeout = 5,
       alert_title = "Низкий заряд батареи!",
       alert_text = "${AC_BAT}${time_est}",
       alert_icon = "~/Downloads/low_battery_icon.png",
       warn_full_battery = true,
       full_battery_icon = "~/Downloads/full_battery_icon.png",
    }

    -- Create a tasklist widget
    s.tasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        },
	style = {
	  shape_border_width = 2,
          shape_border_color = '#a89984',
          shape  = gears.shape.rounded_bar,
	},
	layout = {
	   spacing = 5,
	   spacing_widget = {
	      {
		 forced_width = 5,
		 shape = gears.shape.circle,
		 widget = wibox.widget.separator,
	      },
	      valign = 'center',
	      halign = 'center',
	      widget = wibox.container.widget
	   },
	   layout = wibox.layout.flex.horizontal
	},
    }


-- Underline widget example
    --{{textclock,
--		 bottom = "2",
--		 color = "#98971a",
--		 widget = wibox.container.margin
--		 },
--		   left = 3,
--		   right = 3,
--		   layout = wibox.container.margin
--		},


    -- Create the wibox
    s.wibox = awful.wibar {
        position = "top",
	height = "20",
	bg = beautiful.bg_normal .. "95", -- Transparency level
	screen   = s,
        widget   = {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                launcher,
                taglist,
                s.promptbox,
            },
            s.tasklist, -- Middle widget
            { -- Right widgets
	        layout = wibox.layout.fixed.horizontal,
                keyboardlayout,
		sep,
		wibox.widget.systray(),
		volume,
		sep,
		battery,
		sep,
		{{brightness,
		 bottom = "2",
		 color = "#d65d0e",
		 widget = wibox.container.margin
		 },
		   left = 3,
		   right = 3,
		   layout = wibox.container.margin
		},
		sep,
		{{textclock,
		 bottom = "2",
		 color = "#98971a",
		 widget = wibox.container.margin
		 },
		   left = 3,
		   right = 3,
		   layout = wibox.container.margin
		},
		s.layoutbox,
            },
        }
    }
end)

-- }}}

-- {{{ Mouse bindings
awful.mouse.append_global_mousebindings({
    awful.button({ }, 3, function () mainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext),
})
-- }}}

-- {{{ Key bindings
-- Keycode example for "XK_s"
    --awful.key({ modkey,           }, "#39",      hotkeys_popup.show_help,
              --{description="show help", group="awesome"}),

-- General Awesome keys
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "w", function () mainmenu:show() end,
              {description = "show main menu", group = "awesome"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "x", awesome.quit,
              {description = "quit awesome", group = "awesome"}),
    awful.key({ modkey            }, "b", function () mouse.screen.wibox.visible = not mouse.screen.wibox.visible end,
              {description = "hide bar", group = "awesome"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().promptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey },            "r",     function () awful.screen.focused().promptbox:run() end,
              {description = "run prompt", group = "launcher"}),
    awful.key({ modkey }, "d", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),
})

-- Tags related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),
})

-- Focus related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
	    bling.module.flash_focus.flashfocus(client.focus)
	end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
	    bling.module.flash_focus.flashfocus(client.focus)
	end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:activate { raise = true, context = "key.unminimize" }
                  end
              end,
              {description = "restore minimized", group = "client"}),
})

-- Custom
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,           }, "g", function () lain.util.useless_gaps_resize(1) end,
              {description = "increase gaps size", group = "custom"}),
    awful.key({ modkey, "Shift"   }, "g", function () lain.util.useless_gaps_resize(-1) end,
              {description = "decrease gaps size", group = "custom"}),
    awful.key({ 0,         }, "XF86MonBrightnessUp", function () brightness_widget:inc() end, {description = "increase brightness", group = "custom"}),
    awful.key({ 0,         }, "XF86MonBrightnessDown", function () brightness_widget:dec() end, {description = "decrease brightness", group = "custom"}),

})

-- Layout related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),
})

-- Screenshot
local function saved_screenshot(args)
    local ss = awful.screenshot(args)

    local function notify(self)
        naughty.notification {
            title     = self.file_name,
            message   = "Screenshot saved",
            icon      = self.surface,
            icon_size = 128,
        }
    end

    if args.auto_save_delay > 0 then
        ss:connect_signal("file::saved", notify)
    else
        notify(ss)
    end

    return ss
end

local function delayed_screenshot(args)
    local ss = saved_screenshot(args)
    local notif = naughty.notification {
        title   = "Screenshot in:",
        message = tostring(args.auto_save_delay) .. " seconds"
    }

    ss:connect_signal("timer::tick", function(_, remain)
        notif.message = tostring(remain) .. " seconds"
    end)

    ss:connect_signal("timer::timeout", function()
        if notif then notif:destroy() end
    end)

    return ss
end

-- Screenshot
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({modkey}, "Print",
            function (c) saved_screenshot { auto_save_delay = 0, client = c } end ,
            {description = "take client screenshot", group = "screenshot"}),
        awful.key({modkey, "Control"}, "Print",
            function (c) saved_screenshot { auto_save_delay = 0, interactive = true, client = c } end ,
            {description = "take interactive client screenshot", group = "screenshot"}),
        awful.key({modkey, "Shift"}, "Print",
            function (c) delayed_screenshot { auto_save_delay = 5, client = c } end ,
            {description = "take screenshot in 5 seconds", group = "screenshot"}),
        awful.key({modkey, "Shift", "Control"}, "Print",
            function (c) delayed_screenshot { auto_save_delay = 5, interactive = true, client = c } end ,
            {description = "take interactive screenshot in 5 seconds", group = "screenshot"}),
    })
end)

awful.keyboard.append_global_keybindings({
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numrow",
        description = "only view tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                tag:view_only()
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, "Control" },
        keygroup    = "numrow",
        description = "toggle tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end,
    },
    awful.key {
        modifiers = { modkey, "Shift" },
        keygroup    = "numrow",
        description = "move focused client to tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, "Control", "Shift" },
        keygroup    = "numrow",
        description = "toggle focused client on tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numpad",
        description = "select layout directly",
        group       = "layout",
        on_press    = function (index)
            local t = awful.screen.focused().selected_tag
            if t then
                t.layout = t.layouts[index] or t.layout
            end
        end,
    }
})

client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({
        awful.button({ }, 1, function (c)
            c:activate { context = "mouse_click" }
        end),
        awful.button({ modkey }, 1, function (c)
            c:activate { context = "mouse_click", action = "mouse_move"  }
        end),
        awful.button({ modkey }, 3, function (c)
            c:activate { context = "mouse_click", action = "mouse_resize"}
        end),
    })
end)

client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({ modkey,           }, "f",
            function (c)
                c.fullscreen = not c.fullscreen
                c:raise()
            end,
            {description = "toggle fullscreen", group = "client"}),
        awful.key({ modkey, "Shift"   }, "q",      function (c) c:kill()                         end,
                {description = "close", group = "client"}),
        awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
                {description = "toggle floating", group = "client"}),
        awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
                {description = "move to master", group = "client"}),
        awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
                {description = "move to screen", group = "client"}),
        awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
                {description = "toggle keep on top", group = "client"}),
        awful.key({ modkey,           }, "n",
            function (c)
                -- The client currently has the input focus, so it cannot be
                -- minimized, since minimized clients can't have the focus.
                c.minimized = true
            end ,
            {description = "minimize", group = "client"}),
        awful.key({ modkey,           }, "m",
            function (c)
                c.maximized = not c.maximized
                c:raise()
            end ,
            {description = "(un)maximize", group = "client"}),
        awful.key({ modkey, "Control" }, "m",
            function (c)
                c.maximized_vertical = not c.maximized_vertical
                c:raise()
            end ,
            {description = "(un)maximize vertically", group = "client"}),
        awful.key({ modkey, "Shift"   }, "m",
            function (c)
                c.maximized_horizontal = not c.maximized_horizontal
                c:raise()
            end ,
            {description = "(un)maximize horizontally", group = "client"}),
    })
end)

-- }}}

-- {{{ Rules
-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            focus     = awful.client.focus.filter,
            raise     = true,
            screen    = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Arandr", "Blueman-manager", "Gpick", "Kruler", "Nsxiv",
                "Tor Browser", "Wpa_gui", "veromix", "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    }

    -- Add titlebars to normal clients and dialogs
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = true      }
    }

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- ruled.client.append_rule {
    --     rule       = { class = "Firefox"     },
    --     properties = { screen = 1, tag = "2" }
    -- }
end)
-- }}}

-- {{{ Titlebars
-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = {
        awful.button({ }, 1, function()
            c:activate { context = "titlebar", action = "mouse_move"  }
        end),
        awful.button({ }, 3, function()
            c:activate { context = "titlebar", action = "mouse_resize"}
        end),
    }

    awful.titlebar(c).widget = {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },

	{ -- Middle
            { -- Title
	        halign = "center",
		widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },

	{ -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)
-- }}}

-- {{{ Autostart
awful.spawn({"lf -server"})
--}}}


-- {{{ Notifications
ruled.notification.connect_signal('request::rules', function()
    -- All notifications will match this rule.
    ruled.notification.append_rule {
        rule       = { },
        properties = {
            screen           = awful.screen.preferred,
            implicit_timeout = 5,
        }
    }
end)

naughty.connect_signal("request::display", function(n)
    naughty.layout.box { notification = n }
end)

-- }}}

-- Windows rounding
client.connect_signal("manage",
function(c)
    c.shape = function(cr, w, h)
        gears.shape.rounded_rect(cr, w, h, 10)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)
