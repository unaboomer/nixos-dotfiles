{pkgs ? import <nixpkgs> {}}:
# nix-build
pkgs.stdenv.mkDerivation {
  name = "dmenu";
  src = pkgs.fetchFromGitLab {
    owner = "unaboomer";
    repo = "dmenu";
    rev = "db00c83b7b51ac1f7c63f91666fc4554c876113d";
    hash = "sha256-atnOGCNdebk+Bt+k/Xsa9DwpUEF0k/0+gakVVWctQwE=";
  };
  buildInputs = with pkgs; [
    xorg.libXft
    xorg.libX11
    xorg.libXinerama
    zlib
    fontconfig
  ];
  makeFlags = [ "CC:=$(CC)" ];
  
  installFlags = [ "PREFIX=$(out)" ];
  
  installTargets = [ "install" ];
}
