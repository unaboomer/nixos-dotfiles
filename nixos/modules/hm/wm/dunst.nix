{...}: {
# Dunst
services.dunst = {
  enable = true;
  settings = {
    global = {
      monitor = 0;
      follow = "keyboard";
      width = 370;
      height = 350;
      enable_recursive_icon_lookup = true;
      icon_path = ".local/share/icons/Gruvbox-Plus-Dark";
      notification_limit = 5;
      offset = "0x19";
      stack_duplicates = true;
      show_indicators = true;
      progress_bar = true;
      padding = 2;
      horizontal_padding = 2;
      transparency = 10;
      font = "Iosevka Nerd Font 14";
      format = ''<b>%s</b>\n%b'';
    };
    urgency_low = {
      foreground = "#1d2021";
      background = "#928374";
      timeout = 3;
    };
    urgency_normal = {
      foreground = "#ebdbb2";
      background = "#458588";
      timeout = 5;
    };
    urgency_critical = {
      background = "#cc241d";
      foreground = "#ebdbb2";
      frame_color = "#fabd2f";
      timeout = 10;
    };
  };
};
}
