{pkgs, ...}: {
# Picom
services.picom = {
  enable = true;
  package = pkgs.picom-next;
  backend = "glx";
  vSync = true;
  # Opacity
  activeOpacity = 1.0;
  inactiveOpacity = 1.0;
  # Fade
  fade = true;
  fadeSteps = [0.028 0.03]; # In and Out
  # Shadow
  shadow = false;
  shadowOpacity = 0.75;
  shadowExclude = [
    "class_g ?= 'polybar'"
  ];
  extraArgs = [
    # shadow
    "--shadow-radius 12"
    "--use-damage"
    #"--animation-clamping" # Without --animations
  ];
};
}
