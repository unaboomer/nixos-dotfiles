{pkgs, ...}: {
# Polybar
services.polybar = {
  enable = true;
  script = ''
    polybar top &
  '';
  settings = {
    "colors" = {
      background = "#282828";
      background-alt = "#3c3836";
      foreground = "#ebdbb2";
      primary = "#ebdbb2";
      secondary = "#458488";
      alert = "#cc241d";
      disabled = "#a89984";
    };
    "bar/example" = {
      modules-left = "xworkspaces xwindow";
      modules-right = "keyboard battery volume memory cpu date tray";
      cursor-click = "pointer";
      cursor-scroll = "ns-resize";
      enable-ipc = true;
      wm-restack = "bspwm";
      override-redirect = true;
      width = "100%";
      height = "18pt";
      radius = "0";
      dpi = "96";
      background = "#282828";
      foreground = "#ebdbb2";
      line-size = "2pt";
      border-size = "0pt";
      padding-left = "0";
      padding-right = "0";
      module-margin = "0"; # ?????
      margin-top = "0";
      margin-bottom = "0";
      separator = "|";
      separator-foreground = "#a89984";
      font-0 = "Iosevka Nerd Font:style=regular:size=14;2";
    };
    "module/tray" = {
      type = "internal/tray";
      tray-size = "88%";
    };
    "module/xworkspaces" = {
      type = "internal/xworkspaces";
      label-active = "%name%";
      label-active-padding = "1";
      label-active-background = "#3c3836";
      label-active-underline = "#ebdbb2";
      label-occupied = "%name%";
      label-occupied-padding = "1";
      label-urgent = "%name%";
      label-urgent-background = "#cc241d";
      label-urgent-paddint = "1";
      label-empty = "%name%";
      label-empty-foreground = "#a89984";
      label-empty-padding = "1";
    };
    "module/xwindow" = {
      type = "internal/xwindow";
      label = "%title%";
    };
    "module/keyboard" = {
      type = "internal/xkeyboard";
      blacklist-0 = "num lock";
      label-layout = "%layout%";
      label-layout-foreground = "#ebdbb2";
      label-indicator-padding = "2";
      label-indicator-margin = "1";
      label-indicator-foreground = "#282828";
      label-indicator-background = "#458488";
    };
    "module/battery" = {
      type = "custom/script";
      interval = 1;
      exec = pkgs.writeShellScript "db_battery" ''
               capacity=$(cat /sys/class/power_supply/BAT?/capacity 2>&1)
               for battery in /sys/class/power_supply/bat?*; do
               case "$(cat /sys/class/power_supply/BAT?*/status)" in
                 "Full") status="" ;;
          "Discharging") status="󱊢" ;;
          "Charging") status="󰚥" ;;
          "Not charging") status="󱟩" ;;
          *) exit 1 ;;
               esac
               printf "%s%s%d%%" "$status" "$warn" "$capacity" ; unset warn
               done && printf "\\n"
      '';
      click-left = "notify-send $(acpi -b)";
    };
    "module/volume" = {
      type = "custom/script";
      interval = 1;
      exec = pkgs.writeShellScript "db_sound" ''
         vol="$(wpctl get-volume @DEFAULT_AUDIO_SINK@)"
          # If muted, print 🔇 and exit.
          [ "$vol" != "''${vol%\[MUTED\]}" ] && echo "󱃓 0" && exit
          vol="''${vol#Volume: }"
          split() {
            # For ommiting the . without calling and external program.
            IFS=$2
            set -- $1
            printf '%s' "$@"
          }
          vol="$(printf "%.0f" "$(split "$vol" ".")")"
          case 1 in
            $((vol >= 100)) ) icon="󰪥 " ;;
            $((vol >= 89)) ) icon="󰪤 " ;;
            $((vol >= 78)) ) icon="󰪣 " ;;
            $((vol >= 67)) ) icon="󰪢 " ;;
            $((vol >= 56)) ) icon="󰪡 " ;;
            $((vol >= 45)) ) icon="󰪠 " ;;
            $((vol >= 34)) ) icon="󰪟 " ;;
            $((vol >= 23)) ) icon="󰪞 " ;;
            *) echo "󱃓 0" && exit ;;
          esac
          echo "$icon$vol%"
      '';
      click-left = "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+";
      click-middle = "alacritty -e alsamixer --card=0 --view=all";
      click-right = "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-";
    };
    "module/memory" = {
      type = "internal/memory";
      interval = "2";
      format-prefix = "󰧑 ";
      format-prefix-foreground = "#ebdbb2";
      label = "%percentage_used:1%%";
    };
    "module/cpu" = {
      type = "internal/cpu";
      interval = "2";
      format-prefix = " ";
      format-prefix-foreground = "#ebdbb2";
      label = "%percentage:1%%";
    };
    "module/date" = {
      type = "internal/date";
      interval = "1";
      date = "%H:%M";
      date-alt = "%Y-%m-%d %H:%M:%S";
      label = "%date%";
      label-foreground = "#ebdbb2";
    };
    "settings" = {
      screenchange-reload = "true";
      pseudo-transparency = "true";
    };
  };
};
}
