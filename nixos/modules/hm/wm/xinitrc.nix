{...}: {
# Xinitrc
home.file = {
  ".config/X11/xinitrc" = {
    text = ''
# Basic
xsetroot -cursor_name left_ptr &
# BSPWM #
exec bspwm
# AWESOME WM #
#exec awesome
    '';
  };
};
}
