{...}:
{
environment.shellAliases = {
  cp = "nocorrect rsync -r -h --info=progress2 --info=name0 --preallocate --archive --verbose --compress --partial --progress";
  mv = "nocorrect rsync -r -h --info=progress2 --info=name0 --preallocate --archive --verbose --compress --partial --progress --remove-source-files";
  #nixsys = "doas nixos-rebuild -j 4 switch --flake /etc/nixos/#unaboomer";
  nixsys = "nh os switch -H unaboomer /etc/nixos";
  nix-search = "nix-search -d";
  nix-update = "doas nix flake update ; nixsys";
  find = "fd";
  su = "doas -s";
  sudo = "doas";
  sudoedit = "doasedit";
};
}
