{...}:

{
boot = {
  # Bootloader
  loader = {
    systemd-boot.enable = true;
    efi= {
      canTouchEfiVariables = true;
    };
  };
  # Kernel parameters
  kernelParams = ["atkbd.reset=1"];
  # /tmp cleaning on boot
  tmp = {
    useTmpfs = true;
    cleanOnBoot = true;
  };
};
}
