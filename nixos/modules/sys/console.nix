{...}:
{
# Getty
services.getty = {
  autologinUser = "andrew";
};
# tty setup
console = {
  font = "cyr-sun16";
  keyMap = "us";
  colors = [
    # Normal
    "1D2021" # Black
    "cc251d" # Red
    "98971a" # Green
    "d79921" # Yellow
    "458588" # Blue
    "b16286" # Magenta
    "689d6a" # Cyan
    "a89984" # White
    # Bright
    "928374" # Black
    "fb4934" # Red
    "b8bb26" # Green
    "fabd2f" # Yellow
    "83a598" # Blue
    "d3869b" # Magenta
    "8ec07c" # Cyan
    "ebdbb2" # White
  ];
};
}
