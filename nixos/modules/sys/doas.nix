{pkgs, ...}:
{
# SUDO #
# Needed by 'nh' 
security.sudo = {
  enable = true;
  wheelNeedsPassword = false;
};
# DOAS #
security.doas = {
  enable = true;
  wheelNeedsPassword = false;
  extraRules = [
    { 
      groups = [ "wheel" ];
      cmd = "${pkgs.brightnessctl}/bin/brightnessctl";
      noPass = true;
    }
  ];
};
}
