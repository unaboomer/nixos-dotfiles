{pkgs, ...}:
{
fonts = {
  packages = with pkgs; [
    (nerdfonts.override {fonts = ["Iosevka"];})
  ];
  fontconfig = {
    enable = true;
    defaultFonts = {
      emoji = [ "Iosevka Nerd Font" ];
      monospace = [ "Iosevka Nerd Font Mono" ];
      serif = [ "Iosevka Nerd Font SemiBold" ];
      sansSerif = [ "Iosevka Nerd Font SemiBold" ];
    };
  };
};
}
