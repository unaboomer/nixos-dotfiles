{...}:

{
# Setup hardware
hardware = {
  # Bluetooth
  bluetooth = {
    enable = true;
    powerOnBoot = true;
    # A2DP sink
    settings = {
      General = {
        Enable = "Source,Sink,Media,Socket";
        Experimental = true; # For showing battery charge
      };
    };
  };
  # Opengl
  opengl = {
    enable = true;
    driSupport = true;
  };
};
}
