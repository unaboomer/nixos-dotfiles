{...}:

{
# Use 'psk-gen' to generate config
# Insert psk to 'pskRaw'
networking = {
  hostName = "nixos";
  # Ethernet
  dhcpcd = {
    enable = true;
  };
  # wpa_supplicant
  wireless = {
    enable = true;
    userControlled.enable = true; # Allow 'wpa_cli' command
    networks = {
      RT-GPON-62E3 = {
        pskRaw = "e4ccee812fecbb20904b05c7d92a67e351390afc8bcbd41ccef8492fef928b5c";
      };
      pobd_14_3 = {
        pskRaw = "0719308963dfe02e4234587b11440fd76039242bcec964553daf6fd0fb4dde1d";
      };
    };
  };
};
}
