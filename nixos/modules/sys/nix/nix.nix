{...}:

{
# Nix settings
nix = {
  # Settings (nix.conf) #
  settings = {
    use-xdg-base-directories = true;
    experimental-features = [
      "nix-command"
      "flakes"
    ];
    trusted-users = [
      "root"
      "andrew"
      "@wheel"
    ];
    auto-optimise-store = true;
  };
  # Garbage collection #
  # Storage optimisation
  optimise = {
    automatic = true;
    dates = ["13:00"];
  };
  # Timer for garbage collection
  gc = {
    automatic = true;
    dates = "daily";
    options = "--delete-older-than 1d";
  };
};
}
