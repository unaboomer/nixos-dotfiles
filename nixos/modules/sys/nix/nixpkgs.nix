{lib,  ...}:

{
nixpkgs = {
  config = {
    allowUnfree = false;
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        # Add additional package names here
        "steam-original"
        "steam-run"
    ];
  };
};

}
