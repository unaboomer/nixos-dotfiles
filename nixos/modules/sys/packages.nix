{pkgs,...}:
{
# List packages installed in system profile. To search, run:
# $ nix search wget
environment.systemPackages = with pkgs; [
  # Xorg
  xorg.xorgserver
  xorg.xkill
  xorg.libX11
  fontconfig
  xorg.libXinerama
  xorg.libXft
  xorg.xf86inputsynaptics
  xorg.xf86inputlibinput
  xorg.xf86videointel
  xorg.xf86videoati
  # WM
  libnotify
  brightnessctl
  maim
  # Nix related
  home-manager
  nh nom nvd
  nix-tree
  nurl
  nix-search-cli
  steam-run # Run in FHS evironment
  # Wifi
  wpa_supplicant_gui
  # Other
  fzf
  ripgrep-all
  fd
  xsel
  xclip
  gnumake
  gcc
  git
  aria2
  ffmpeg
  busybox
  file
  acpi
  curl
  htop-vim
];
}
