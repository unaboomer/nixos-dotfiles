{... }:

{
fileSystems."/persist".neededForBoot = true;
environment.persistence."/persist/system" = {
  hideMounts = true;
  directories = [
    "/etc/nixos"
    "/var/log"
    "/var/lib/bluetooth"
    "/var/lib/nixos"
    "/var/lib/systemd/coredump"
    "/etc/NetworkManager/system-connections"
  ];
  files = [
    "/etc/machine-id"
    { file = "/etc/nix/id_rsa"; parentDirectory = { mode = "u=rwx,g=,o="; }; }
  ];
};

programs.fuse.userAllowOther = true;

}
