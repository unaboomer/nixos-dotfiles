{...}:
{
# Pipewire
security.rtkit.enable = true;
services.pipewire = {
  enable = true;
  # Wireplumber session manager 
  wireplumber.enable = true;
  # Alsa support
  alsa = {
    enable = true;
    support32Bit = true;
  };
  # Pulseaudio support
  pulse.enable = true;
  # Jack support
  jack.enable = true;
};
}
