{pkgs, ...}: let
  # Lf wrapper #
  lfub = pkgs.writeShellScriptBin "lfub" ''
set -e
if [ -n "$DISPLAY" ]; then
  export FIFO_UEBERZUG="''${TMPDIR:-/tmp}/lf-ueberzug-$$"
  cleanup() {
    exec 3>&-
    rm "$FIFO_UEBERZUG"
  }
  mkfifo "$FIFO_UEBERZUG"
  ueberzug layer -s <"$FIFO_UEBERZUG" &
  exec 3>"$FIFO_UEBERZUG"
  trap cleanup EXIT
  if ! [ -d "$XDG_CACHE_HOME/lf" ]; then
    mkdir -p "$XDG_CACHE_HOME/lf"
  fi
  lf "$@" 3>&-
else
  exec lf "$@"
fi
  '';
  # Doasedit #
  doasedit = pkgs.writeShellScriptBin "doasedit" ''
    help() {
    	cat - >&2 <<EOF
    doasedit - like sudoedit, but for doas

    doasedit [file]

    Every argument will be treated as a file to edit. There's no support for
    passing arguments to doas, so you can only doas root.

    This script is SECURITY SENSITIVE! Special care has been taken to correctly
    preserve file attributes. Please exercise CAUTION when modifying AND using
    this script.

    Creator: https://github.com/AN3223
    EOF
    }
    case "$1" in --help|-h) help; exit 0;; esac

    export TMPDIR=/dev/shm/
    trap 'trap - EXIT HUP QUIT TERM INT ABRT; rm -f "$tmp" "$tmpcopy"' EXIT HUP QUIT TERM INT ABRT

    for file; do
    	case "$file" in -*) file=./"$file" ;; esac
            filename="$1"
    	ext=''${filename##*.}
    	tmp="$(mktemp).$ext"
    	if [ -f "$file" ] && [ ! -r "$file" ]; then
    		doas cat "$file" > "$tmp"
    	elif [ -r "$file" ]; then
    		cat "$file" > "$tmp"
    	fi

    	tmpcopy="$(mktemp).$ext"
    	cat "$tmp" > "$tmpcopy"

    	nvim "$tmp"

    	if cmp -s "$tmp" "$tmpcopy"; then
    		echo "Файл не изменён, выхожу..."
    	else
    		doas dd if="$tmp" of="$file"
    		echo "Готово, изменения записаны"
    	fi

    	rm "$tmp" "$tmpcopy"
    done
  '';
  # Format USB for compability with Windows/Android
  mkwinfs = pkgs.writeShellScriptBin "mkwinfs" ''
    lsblk --output NAME,TYPE,SIZE,MOUNTPOINT
    echo ""
    read -r -p "Select Disk to format: " DISK
    echo "$DISK"
    read -e -r -p "Do you want to format disk? [y/n] " choice
    [[ "$choice" == [Yy]* ]] && doas umount /dev/"$DISK" ; doas mkfs.vfat -v -F32 /dev/"$DISK" || exit
  '';
  # wpa_supplicant tui
  psk-gen = pkgs.writeShellScriptBin "psk-gen" ''
read -rp "Enter WIFI name: " SSID
read -rp "Enter WIFI pass: " PASS
# Using wpa_passphrase to generate PSK and extract hash value using awk
PSK_HASH=$(wpa_passphrase "$SSID" "$PASS" | awk -F'=' 'NR==4 {print $2}')
# Using printf to output formatted result
printf '%s = {\n  pskRaw = "%s";\n};\n' "$SSID" "$PSK_HASH"
  '';
in {
  # Script packages
  environment.systemPackages = [
    doasedit 
    lfub 
    mkwinfs
    psk-gen
  ];
}
