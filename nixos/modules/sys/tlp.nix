{...}:
{
# Power Save #
# TLP
services.power-profiles-daemon.enable = false;
services.tlp = {
  enable = true;
  settings = {
    CPU_SCALING_GOVERNOR_ON_AC = "performance";
    CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

    CPU_ENERGY_PERF_POLICY_ON_BAT = "powersave";
    CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
    CPU_BOOST_ON_AC = 1;
    CPU_BOST_ON_BAT = 0;

    PLATFORM_PROFILE_ON_AC = "performace";
    PLATFORM_PROFILE_ON_BAT = "low-power";

    RADEON_DPM_STATE_ON_AC = "performance";
    RADEON_DPM_STATE_ON_BAT = "battery";

    USB_AUTOSUSPEND = 0;
    USB_EXCLUDE_AUDIO = 1;
    USB_EXCLUDE_BTUSB = 1;
    USB_EXCLUDE_PHONE = 1;
    USB_EXCLUDE_PRINTER = 1;
    USB_EXCLUDE_WWAN = 1;
  };
};
}
