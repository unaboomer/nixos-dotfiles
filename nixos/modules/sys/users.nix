{pkgs, ...}:
{
users.users = {
  andrew = {
    isNormalUser = true;
    createHome = true;
    description = "andrew";
    # 'mkpasswd' generate different hash on same password. Its normal
    hashedPassword = "$y$j9T$TpuxkIR3F.BchcMlNBlLp1$GaeHNAGppDAZ4horBqxmujq5vUNMIdE2DpyxyQfVaT8";
    extraGroups = [
      "networkmanager"
      "wheel"
      "audio"
      "video"
      "disk"
      "storage"
      "kvm"
    ];
    shell = pkgs.zsh;
  };
};
}
