{...}:
{
environment.sessionVariables = {
  # shell
  PS1 = "%b%{$fg[magenta]%}[%{$fg[red]%}%n%{$fg[yellow]%}@%{$fg[green]%}%m %{$fg[blue]%}%~%{$fg[magenta]%}]%{$reset_color%}$%b ";
  # For bat
  BAT_THEME = "gruvbox-dark";
  BAT_STYLE = "plain";
  # Nix hack
  NIX_AUTO_RUN = "1";
  # Etc
  LANG = "ru_RU.UTF-8";
  LANGUAGE = "ru_RU.UTF-8";
  LC_CTYPE = "ru_RU.UTF-8";
  LC_ALL = "ru_RU.UTF-8";
  LC_MESSAGES = "ru_RU.UTF-8";
};
}
