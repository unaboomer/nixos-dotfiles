{...}:
{
# Xorg #
services.xserver = {
  enable = true;
  # Autorun
  autorun = true;
  # Mousepad
  libinput = {
    enable = true;
  };
  # Layout
  xkb = {
    layout = "us,ru";
    model = "pc105";
    options = "grp:alt_shift_toggle";
  };
  displayManager.startx.enable = true;
};
}
