{...}:
{
programs.zsh = {
  enable = true;
  enableCompletion = true;
  syntaxHighlighting = {
    enable = true;
  };
  autosuggestions = {
    enable = true;
    async = true;
    extraConfig = {
      ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE = "fg=#98971a,bold,underline";
    };
  };
  enableGlobalCompInit = true;
  promptInit = ''
  autoload -U colors && colors; PS1="%b%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%m %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
  '';
  interactiveShellInit = ''
    # Completion
    autoload -U compinit && compinit
    zstyle ':completion:' menu select
    zmodload zsh/complist
    _comp_options+=(globdots)
    # Suggestion
    export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
    # Prompt
    echo '\e[5 q'
    '';
};
}
